-- MySQL dump 10.13  Distrib 8.0.13, for Linux (x86_64)
--
-- Host: localhost    Database: planestracker
-- ------------------------------------------------------
-- Server version	8.0.13

DROP DATABASE IF EXISTS planestracker;
CREATE DATABASE planestracker;

--CREATE USER IF NOT EXISTS 'planestracker'@'localhost' IDENTIFIED BY 'Epsi2018-2019';
--grant all privileges on planestracker.localhost to 'planestracker'@'*';
--ALTER USER 'planestracker'@localhost IDENTIFIED WITH mysql_native_password BY 'Epsi2018-2019';
CREATE USER 'planestracer'@'%' IDENTIFIED BY 'Epsi2018-2019';
GRANT ALL PRIVILEGES ON *.* TO 'planestracker'@'%' WITH GRANT OPTION;
ALTER USER 'planestracker'@localhost IDENTIFIED WITH mysql_native_password BY 'Epsi2018-2019';
flush privileges;
--update user set Host='%' where user='planestracker';

use planestracker;

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 /* SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Area`
--

DROP TABLE IF EXISTS `Area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `Area` (
  `area_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `lat_min` float NOT NULL,
  `lat_max` float NOT NULL,
  `lon_min` float NOT NULL,
  `lon_max` float NOT NULL,
  PRIMARY KEY (`area_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Area`
--

LOCK TABLES `Area` WRITE;
/*!40000 ALTER TABLE `Area` DISABLE KEYS */;
INSERT INTO `Area` VALUES (1,'Nantes - Area 1',122555,133330,144444,1355),(2,'Nantes - Area 2',122555,133330,144444,1355);
/*!40000 ALTER TABLE `Area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Company`
--

DROP TABLE IF EXISTS `Company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `Company` (
  `company_id` char(3) NOT NULL,
  `substitute_id` char(3) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Company`
--

LOCK TABLES `Company` WRITE;
/*!40000 ALTER TABLE `Company` DISABLE KEYS */;
INSERT INTO `Company` VALUES ('AAF','','Aigle Azur'),('AEA','','Air Europa'),('AFR','','Air France'),('AVA','','Avianca'),('AZA','','Alitalia'),('BAW','CFE','British Airways'),('BEL','','Brussels Airlines'),('CLX','','Cargolux'),('DAH','','Air Algerie'),('DLH','','Lufthansa'),('EIN','','Aer Lingus'),('EWG','','Eurowings'),('EZY','EZS','Easy Jet'),('FIN','','Finnair'),('FWI','','Air Caraïbes'),('GMI','','Germania'),('HOP','','HOP!'),('IBE','IBS','Iberia'),('IBK','NAX','Norwegian'),('JTG','','Jettime'),('KLM','','KLM'),('LGL','','Luxair'),('MAC','','Air Arabia'),('OHY','','Onur Air'),('RAM','','Royal Air Maroc'),('ROU','','Air Canada Rouge'),('RYR','LDM','Ryanair'),('SAS','','Scandinavian Airlines System'),('TAM','','Latam'),('TAP','','TAP Air Portugal'),('TAR','','Tunisair'),('TCV','','Icelandair'),('TFL','JAF','TUI'),('TRA','TVF','Transavia'),('TVS','','Czech Airlines'),('UAE','','Emirates'),('UAL','','United Airlines'),('VDA','','Volga-Dnepr Airlines'),('VKG','CFG','Thomas Cook Airlines'),('VLG','','Vueling'),('VOE','','Volotea'),('WOW','','WOW Air'),('XLF','','XL Airways France');
/*!40000 ALTER TABLE `Company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Plane`
--

DROP TABLE IF EXISTS `Plane`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `Plane` (
  `callsign` varchar(15) NOT NULL,
  `flight_id` int(11) NOT NULL,
  `company_name` varchar(150) DEFAULT NULL,
  `date_flight` datetime DEFAULT NULL,
  `origin_country` varchar(50) NOT NULL,
  PRIMARY KEY (`callsign`,`flight_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Plane`
--

LOCK TABLES `Plane` WRITE;
/*!40000 ALTER TABLE `Plane` DISABLE KEYS */;
INSERT INTO `Plane` VALUES ('AAF315',1,'Aigle Azur','2018-12-12 16:13:01','France'),('AEA13DZ',1,'Air Europa','2018-12-12 16:45:03','Spain'),('AFR162H',1,'Air France','2018-12-12 14:38:32','France'),('AFR179F',1,'Air France','2018-12-12 16:10:01','France'),('AFR368',1,'Air France','2018-12-12 11:39:58','France'),('AFR416',1,'Air France','2018-12-12 14:18:01','France'),('AFR480',1,'Air France','2018-12-12 14:30:52','France'),('AFR48GJ',1,'Air France','2018-12-12 16:44:03','France'),('AFR49HX',1,'Air France','2018-12-12 11:53:43','France'),('AFR62HU',1,'Air France','2018-12-12 13:39:13','France'),('AFR738W',1,'Air France','2018-12-12 14:43:50','France'),('AFR772N',1,'Air France','2018-12-13 10:45:35','France'),('AFR825',1,'Air France','2018-12-13 10:57:30','France'),('AFR842',1,'Air France','2018-12-12 14:11:41','France'),('AFR87JK',1,'Air France','2018-12-12 15:29:39','France'),('AIB01FQ',1,NULL,'2018-12-12 15:50:04','France'),('AZA677',1,'Alitalia','2018-12-12 15:55:25','Ireland'),('AZA677',2,'Alitalia','2018-12-12 16:00:00','Ireland'),('BAW2633',1,NULL,'2018-12-12 13:39:13','United Kingdom'),('BAW45EM',1,NULL,'2018-12-12 14:26:21','United Kingdom'),('BAW473',1,NULL,'2018-12-12 11:39:58','United Kingdom'),('BAW475',1,NULL,'2018-12-12 15:39:35','United Kingdom'),('BAW47MG',1,NULL,'2018-12-12 14:07:50','United Kingdom'),('BAW485',1,NULL,'2018-12-12 16:34:11','United Kingdom'),('BEL3780',1,'Brussels Airlines','2018-12-12 16:09:20','Belgium'),('BEL3AD',1,'Brussels Airlines','2018-12-12 11:41:34','Belgium'),('BEL6NT',1,'Brussels Airlines','2018-12-13 10:45:35','Spain'),('BGA127B',1,NULL,'2018-12-12 14:33:24','France'),('CND392',1,NULL,'2018-12-12 16:41:03','Kingdom of the Netherlands'),('DLH1795',1,'Lufthansa','2018-12-12 16:34:41','Germany'),('DLH33A',1,'Lufthansa','2018-12-13 10:59:05','Germany'),('DLH33C',1,'Lufthansa','2018-12-12 14:27:21','Germany'),('DLH86J',1,'Lufthansa','2018-12-12 15:48:54','Germany'),('EIN50H',1,'Aer Lingus','2018-12-12 14:34:12','Ireland'),('EIN50W',1,'Aer Lingus','2018-12-12 16:08:20','Ireland'),('EIN63PT',1,'Aer Lingus','2018-12-12 11:48:53','Ireland'),('EWG6FR',1,'Eurowings','2018-12-12 16:10:01','Germany'),('EZS19BX',1,'Easy Jet','2018-12-13 10:45:35','Switzerland'),('EZS75YM',1,'Easy Jet','2018-12-12 16:02:15','Switzerland'),('EZS96QE',1,'Easy Jet','2018-12-12 11:42:23','Switzerland'),('EZY12FU',1,'Easy Jet','2018-12-12 16:11:51','Austria'),('EZY13ZP',1,'Easy Jet','2018-12-12 14:29:02','Austria'),('EZY29DV',1,'Easy Jet','2018-12-12 13:39:13','United Kingdom'),('EZY37VT',1,'Easy Jet','2018-12-12 16:48:34','Austria'),('EZY41YX',1,'Easy Jet','2018-12-12 16:44:03','Austria'),('EZY48ED',1,'Easy Jet','2018-12-12 15:59:40','United Kingdom'),('EZY48ED',2,'Easy Jet','2018-12-12 16:00:00','United Kingdom'),('EZY56CD',1,'Easy Jet','2018-12-12 16:23:52','Austria'),('EZY61JF',1,'Easy Jet','2018-12-12 15:33:24','United Kingdom'),('EZY631D',1,'Easy Jet','2018-12-12 16:47:23','Austria'),('EZY65BQ',1,'Easy Jet','2018-12-12 13:44:23','Austria'),('EZY69QZ',1,'Easy Jet','2018-12-12 13:39:43','United Kingdom'),('EZY721Z',1,'Easy Jet','2018-12-12 15:36:44','Austria'),('EZY74LE',1,'Easy Jet','2018-12-12 13:47:23','Austria'),('EZY75JQ',1,'Easy Jet','2018-12-12 16:52:12','United Kingdom'),('EZY78UT',1,'Easy Jet','2018-12-12 15:35:56','Austria'),('EZY8018',1,'Easy Jet','2018-12-12 15:47:04','United Kingdom'),('EZY83BV',1,'Easy Jet','2018-12-12 11:47:43','Austria'),('FWI10A',1,'Air Caraïbes','2018-12-12 13:51:04','France'),('FWWZT',1,NULL,'2018-12-13 10:53:25','France'),('GMI27LK',1,'Germania','2018-12-12 15:29:39','Germany'),('GMI53QD',1,'Germania','2018-12-12 16:37:33','Germany'),('GMI68QM',1,'Germania','2018-12-13 10:50:25','Germany'),('HOP03FF',1,'HOP!','2018-12-12 14:22:31','France'),('HOP58SJ',1,'HOP!','2018-12-12 15:48:25','Croatia'),('HOP694X',1,'HOP!','2018-12-12 14:35:12','Croatia'),('HOP90HL',1,'HOP!','2018-12-12 14:33:52','Germany'),('IBE31MB',1,'Iberia','2018-12-12 15:59:55','Spain'),('IBE31MB',2,'Iberia','2018-12-12 16:00:00','Spain'),('IBK2EP',1,'Norwegian','2018-12-12 14:20:11','Ireland'),('IBK3401',1,'Norwegian','2018-12-12 14:41:52','Ireland'),('IBK5024',1,'Norwegian','2018-12-12 16:34:11','Ireland'),('IBK5051',1,'Norwegian','2018-12-12 14:32:12','Ireland'),('IBK5391',1,'Norwegian','2018-12-12 14:43:45','Ireland'),('IBK5PH',1,'Norwegian','2018-12-12 11:39:58','Ireland'),('IBK6473',1,'Norwegian','2018-12-12 15:45:40','Ireland'),('IBK8PQ',1,'Norwegian','2018-12-12 14:39:22','Ireland'),('JAF2PD',1,'TUI','2018-12-12 16:16:53','Belgium'),('JTG265',1,NULL,'2018-12-12 11:39:58','Denmark'),('KLM1695',1,'KLM','2018-12-12 14:13:41','Kingdom of the Netherlands'),('KLM755',1,'KLM','2018-12-12 11:39:58','Kingdom of the Netherlands'),('LDM5FN',1,'Ryanair','2018-12-12 16:10:30','Ireland'),('LGL3762',1,NULL,'2018-12-12 15:40:24','Luxembourg'),('N623MA',1,NULL,'2018-12-12 16:21:05','United States'),('NAX1WA',1,'Norwegian','2018-12-12 15:56:15','Norway'),('NAX1WA',2,'Norwegian','2018-12-12 16:00:00','Norway'),('NAX83G',1,'Norwegian','2018-12-12 13:39:13','Norway'),('OHY2944',1,NULL,'2018-12-13 10:58:40','Turkey'),('RAM800F',1,'Royal Air Maroc','2018-12-12 15:54:14','Morocco'),('RAM800F',2,'Royal Air Maroc','2018-12-12 16:00:00','Morocco'),('RYR172A',1,'Ryanair','2018-12-12 15:55:01','Ireland'),('RYR172A',2,'Ryanair','2018-12-12 16:00:00','Ireland'),('RYR1GW',1,'Ryanair','2018-12-13 10:50:55','Ireland'),('RYR1SG',1,'Ryanair','2018-12-12 16:11:01','Ireland'),('RYR22MM',1,'Ryanair','2018-12-12 14:36:52','Ireland'),('RYR23VP',1,'Ryanair','2018-12-12 16:54:32','Ireland'),('RYR288U',1,'Ryanair','2018-12-12 15:43:44','Ireland'),('RYR30LY',1,'Ryanair','2018-12-12 16:46:44','Ireland'),('RYR33NK',1,'Ryanair','2018-12-13 10:45:35','Ireland'),('RYR3AK',1,'Ryanair','2018-12-12 16:17:01','Ireland'),('RYR4EC',1,'Ryanair','2018-12-12 16:13:21','Ireland'),('RYR4NY',1,'Ryanair','2018-12-12 14:43:45','Ireland'),('RYR4X',1,'Ryanair','2018-12-12 13:50:03','Ireland'),('RYR4Y',1,'Ryanair','2018-12-12 11:39:58','Ireland'),('RYR58VX',1,'Ryanair','2018-12-12 13:48:23','Ireland'),('RYR5AB',1,'Ryanair','2018-12-12 11:49:23','Ireland'),('RYR5BA',1,'Ryanair','2018-12-12 16:18:51','Ireland'),('RYR63PU',1,'Ryanair','2018-12-12 16:44:13','Ireland'),('RYR66VE',1,'Ryanair','2018-12-12 15:38:14','Ireland'),('RYR67BB',1,'Ryanair','2018-12-12 16:14:41','Ireland'),('RYR67UH',1,'Ryanair','2018-12-12 15:33:24','Ireland'),('RYR6VU',1,'Ryanair','2018-12-12 16:34:11','Ireland'),('RYR89RV',1,'Ryanair','2018-12-12 11:42:13','Ireland'),('RYR8LB',1,'Ryanair','2018-12-12 14:31:13','Ireland'),('RYR9GR',1,'Ryanair','2018-12-13 10:58:40','Ireland'),('RYR9WL',1,'Ryanair','2018-12-12 11:39:58','Ireland'),('SAS2848',1,'Scandinavian Airlines System','2018-12-12 14:09:21','Sweden'),('TAP432M',1,'TAP Air Portugal','2018-12-12 16:02:15','Portugal'),('TAP433B',1,'TAP Air Portugal','2018-12-12 11:39:58','Portugal'),('TAP439B',1,'TAP Air Portugal','2018-12-12 16:24:21','Portugal'),('TAP451',1,'TAP Air Portugal','2018-12-13 10:57:25','Portugal'),('TAP537',1,'TAP Air Portugal','2018-12-12 14:06:50','Portugal'),('TAP643',1,'TAP Air Portugal','2018-12-12 14:22:22','Portugal'),('TAP672',1,'TAP Air Portugal','2018-12-12 15:51:14','Portugal'),('TAP674',1,'TAP Air Portugal','2018-12-13 10:45:35','Portugal'),('TAP691F',1,'TAP Air Portugal','2018-12-12 14:42:32','Portugal'),('TAP747',1,'TAP Air Portugal','2018-12-13 10:57:45','Portugal'),('TCV640',1,NULL,'2018-12-12 15:43:14','Iceland'),('TRA5687',1,'Transavia','2018-12-13 10:45:35','Kingdom of the Netherlands'),('TRA61P',1,'Transavia','2018-12-12 15:33:53','Kingdom of the Netherlands'),('TRA6213',1,'Transavia','2018-12-12 14:01:24','Kingdom of the Netherlands'),('TRA7U',1,'Transavia','2018-12-12 14:24:42','Kingdom of the Netherlands'),('TRA81E',1,'Transavia','2018-12-12 16:02:40','Kingdom of the Netherlands'),('TRA92T',1,'Transavia','2018-12-12 16:55:02','Kingdom of the Netherlands'),('TUI367',1,NULL,'2018-12-12 16:34:11','Germany'),('TUI3X',1,NULL,'2018-12-13 10:48:15','Germany'),('TUI4CW',1,NULL,'2018-12-12 14:35:32','Germany'),('TUI77G',1,NULL,'2018-12-12 16:10:01','Germany'),('TUI7KJ',1,NULL,'2018-12-12 16:42:23','Germany'),('TUI7M',1,NULL,'2018-12-12 14:39:22','Germany'),('TUI7WE',1,NULL,'2018-12-12 13:59:34','Germany'),('TUI7WE',2,NULL,'2018-12-12 14:00:04','Germany'),('TVF64SM',1,'Transavia','2018-12-12 15:48:25','France'),('TVF94KR',1,'Transavia','2018-12-12 16:11:11','France'),('UAE210',1,NULL,'2018-12-12 11:39:58','United Arab Emirates'),('UAE5RQ',1,NULL,'2018-12-13 10:45:35','United Arab Emirates'),('VKG812',1,'Thomas Cook Airlines','2018-12-12 11:39:58','Denmark'),('VLG26N',1,'Vueling','2018-12-12 13:57:24','Spain'),('VLG26N',2,'Vueling','2018-12-12 14:00:04','Spain'),('VLG3189',1,'Vueling','2018-12-12 13:45:13','Spain'),('VOE2641',1,'Volotea','2018-12-12 16:34:21','Spain');
/*!40000 ALTER TABLE `Plane` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE TRIGGER `TRG_PLANE_AUTOINC` BEFORE INSERT ON `Plane` FOR EACH ROW BEGIN
  DECLARE id_autoinc int;
  SET id_autoinc = (SELECT MAX(flight_id) FROM Plane WHERE callsign = NEW.callsign);
  IF (id_autoinc IS NULL) THEN
    SET id_autoinc = 1;
  ELSE
    SET id_autoinc = id_autoinc + 1;
  END IF;
  SET NEW.flight_id = id_autoinc;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE TRIGGER `TRG_PLANE_COMP` BEFORE INSERT ON `Plane` FOR EACH ROW BEGIN
  DECLARE company varchar(150);
  DECLARE id char(3);

  SET id = LEFT(NEW.callsign, 3);
  SET company = (SELECT name FROM Company WHERE (company_id = id OR substitute_id = id));
  SET NEW.company_name = company;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-15 18:48:58
