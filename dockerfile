FROM debian:stretch

RUN apt-get update \
&& apt-get install -y curl \
&& rm -rf /var/lib/apt/lists/*

RUN cd /tmp \
&& wget https://dev.mysql.com/get/mysql-apt-config_0.8.10-1_all.deb \
&& sudo dpkg -i mysql-apt-config* \
&& sudo apt update \
&& sudo apt install mysql-server
# MySQL
WORKDIR /usr/src/planestracker/SQL
COPY ./SQL/* .

RUN mysql -u root -p -h localhost < dump.sql

# Create app directory
WORKDIR /usr/src/planestracker/API

# Install dependencies
COPY package*.json ./

RUN npm install

# Add source code
COPY ./API/ .

EXPOSE 8081

# Run Server
CMD ["npm", "start"]
