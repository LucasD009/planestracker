import { Component } from '@angular/core';
import {Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { _getViewData } from '@angular/core/src/render3/instructions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'PlanesTracker';
  private apiUrl = 'http://192.168.43.119:8081/planes';
  dataCie : any = {};
  dataTime : any = {};
  
  constructor(private http: Http){
    console.log('Connexion API OK');
    this.getDataByCompanies();
    this.getPlanesByCompanies();
    this.getDataByTimeScale();
    this.getPlanesByTimeScale();
  }
  
  getDataByCompanies() {
    return this.http.get(this.apiUrl + "/byCompanies")
    .map((res: Response) => res.json());
  }

  getPlanesByCompanies() {
    this.getDataByCompanies().subscribe(dataCie => {
      console.log(dataCie);
      this.dataCie=dataCie
    })
  }

  getDataByTimeScale() {
    return this.http.get(this.apiUrl + "/byTimeScale")
    .map((res: Response) => res.json());
  }

  getPlanesByTimeScale() {
    this.getDataByTimeScale().subscribe(dataTime => {
      console.log(dataTime);
      this.dataTime=dataTime
    })
  }
}
