var Area = require('../model/areaModel');

exports.getAllAreas = function(req, res){
  Area.getAllAreas(function(err, area){
    if(err){
      res.send(err);
    }
    res.send(area);
  });
};

exports.createArea = function(req, res){
  var newArea = new Area(req.body);
  if(!newArea.lat_min || !newArea.lat_max || !newArea.lon_min || !newArea.lon_max){
    res.status(400).send({
      error: true,
      message: 'Latitude min, max and longitude min, max are required'
    });
  }else{
    Area.createArea(newArea, function(err, area){
      if(err){
        res.send(err);
      }
      res.json(area);
    });
  }
};

exports.getArea = function(req, res){
  Area.getArea(req.params.area_id, function(err, area){
    if(err){
      res.send(err);
    }
    res.json(area);
  });
};

exports.updateArea = function(req, res){
  Area.updateArea(req.params.area_id, new Area(req.body), function(err, area){
    if(err){
      res.send(err);
    }
    res.json(area);
  });
};

exports.deleteArea = function(req, res){
  Area.deleteArea(req.params.area_id, function(err, area){
    if(err){
      res.send(err);
    }
    res.json(plane);
  });
};
