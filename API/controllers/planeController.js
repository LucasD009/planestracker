var Plane = require('../model/planeModel');

exports.getAllPlanes = function(req, res){
  Plane.getAllPlanes(function(err, plane){
    if(err){
      console.log('Controller Plane - getAllPlanes - Error : \n', err);
      res.send(err);
    }
    res.send(plane);
  });
};

exports.createPlane = function(req, res){
  var newPlane = new Plane(req.body);
  if(!newPlane.callsign){
    console.log('Controller Plane - createPlane - Error : callsign is missing');
    res.status(400).send({
      error: true,
      message: 'Callsign is required'
    });
  }else{
    Plane.createPlane(newPlane, function(err, plane){
      if(err){
        console.log('Controller Plane - createPlane - Error : \n', err);
        res.send(err);
      }
      res.json(plane);
    });
  }
};

exports.getPlane = function(req, res){
  Plane.getPlane(req.params, function(err, plane){
    if(err){
      console.log('Controller Plane - getPlane - Error : \n', err);
      res.send(err);
    }
    res.json(plane);
  });
};

exports.getAllPlanesByCompanies = function(req, res){
  Plane.getAllPlanesByCompanies(function(err, planes){
    if(err){
      console.log('Controller Plane - getAllPlanesByCompanies - Error : \n', err);
      res.send(err);
    }
    console.log('Controller Plane - getAllPlanesByCompanies - Result : \n', planes);
    res.send({
      "companies":planes
    });
  });
};

exports.getAllPlanesByTimeScale = function(req, res){
  Plane.getAllPlanesByTimeScale(function(err, planes){
    if(err){
      console.log('Controller Plane - getAllPlanesByTimeScale - Error : \n', err);
      res.send(err);
    }
    console.log('Controller Plane - getAllPlanesByTimeScale - Result : \n', planes);
    res.send({
      "time":planes
    });
  });
};
