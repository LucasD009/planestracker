module.exports = function(app){

  var planeController = require('../controllers/planeController');
  var areaController  = require('../controllers/areaController');

  // Planes routes
  app.route('/planes')
    .get(planeController.getAllPlanes)
    .post(planeController.createPlane);

  app.route('/plane/:callsign/:date_flight/:time')
    .get(planeController.getPlane);

  app.route('/planes/byCompanies')
    .get(planeController.getAllPlanesByCompanies);

  app.route('/planes/byTimeScale')
    .get(planeController.getAllPlanesByTimeScale);

    // Areas routes
  app.route('/areas')
    .get(areaController.getAllAreas)
    .post(areaController.createArea);

  app.route('/area/:area_id')
    .get(areaController.getArea)
    .put(areaController.updateArea)
    .delete(areaController.deleteArea);
};
