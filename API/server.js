const express     = require('express'),
  app             = express(),
  cors            = require('cors'),
  router          = express.Router(),
  bodyParser      = require('body-parser'),
  port            = process.env.PORT || 8081;

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var routes = require('./routes/routes');
routes(app);

app.listen(port);

console.log('Server listening on port ' + port);
console.log('CORS-enabled web server listening on port ' + port);
