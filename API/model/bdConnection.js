/**
  *  Connection à la base de données MySQL
  *   Epsi2018-2019
*/

var mysql = require('mysql');
//var mysql = require('@mysql/xdevapi');

var connection = mysql.createConnection({
  host: "localhost",//"192.168.0.2",
  port: "3306",
  user: "planestracker",
  password: "Epsi2018-2019",
  database: 'planestracker'
});

connection.connect(function(err){
  if(err){
    console.log("MySQL Connection - Fail");
    console.log(err);
  }else{
    console.log("MySQL Connection - Success");
  }
});
// const options = {
//     host: "localhost",//"192.168.0.2",
//     user: "planestracker",
//     password: "Epsi2018-2019",
//     schema: "planestracker"
// };
//
// var session = mysql.getSession(options)
//   .then(session => {
//     console.log("MySQL Connection - Success");
//     return session
//   })
//   .catch(err => {
//     console.error(err.stack);
//   });

module.exports = {
  getDbConnection: connection
};
