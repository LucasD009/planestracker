var bd = require('./bdConnection');
var sql = bd.getDbConnection;

var Area = function(area){
  this.area_id = area.area_id;
  this.name = area.name;
  this.lat_min = area.lat_min;
  this.lat_max = area.lat_max;
  this.lon_min = area.lon_min;
  this.lon_max = area.lon_max;
};

Area.getAllAreas = function getAllAreas(result){
  sql.query("SELECT * FROM Area", function(err, res){
    if(err){
      console.log("SQL error - getAllAreas: ", err);
    } else{
      console.log('areas: ', res);
      result(null, res);
    }
  });
};

Area.createArea = function createArea(newArea, result){
  sql.query("INSERT INTO Area set ?", newArea, function(err, res){
    if(err){
      console.log("SQL error - createArea", err);
    } else{
      console.log('area: ', res);
      result(null, res.insertId);
    }
  });
};

Area.getArea = function getArea(area_id, result){
  sql.query("SELECT * FROM Area WHERE area_id = ?", area_id, function(err, res){
    if(err){
      console.log("SQL error - getArea: ", err);
      result(err, null);
    }else{
      console.log("area: ", res);
      result(null, res);
    }
  });
};

Area.updateArea = function updateArea(area, result){
  sql.query("UPDATE Area SET ", [area], function(err, res){
    if(err){
      console.log("SQL error - updateArea: ", err);
      result(err, null);
    }else{
      console.log("area: ", res);
      result(null, res);
    }
  });
};

Area.deleteArea = function deleteArea(area_id, result){
  sql.query("DELETE FROM Area WHERE area_id = '?'", area_id, function(err, res){
    if(err){
      console.log("SQL error - deleteArea: ", err);
      result(err, null);
    }else{
      console.log("area: ", res);
      result(null, res);
    }
  });
};

module.exports = Area;
