var bd = require('./bdConnection');
var request = require("request");
var sql = bd.getDbConnection;


// Plane structure
var Plane = function(plane){
  this.callsign = plane.callsign;
  this.date_flight = plane.date_flight;
  this.origin_country = plane.origin_country;
};

// Route that retrieve all planes
Plane.getAllPlanes = function getAllPlanes(result){
  // var res = sql.then(s => s.getSchema('planestracker').getTable('Plane').select().execute(
  //   function (row) {
  //     console.log(row);
  //   }
  // ));
  // result(null, res);
  sql.query("SELECT * FROM Plane", function(err, res){
    if(err){
      console.log("Model Plane - getAllPlanes - Error : \n", err);
      result(err, null);
    } else {
      console.log("Model Plane - getAllPlanes - Result : \n", res);
      result(null, res);
    }
  });
};

// Route that create plane
Plane.createPlane = function createPlane(newPlane, result) {
  sql.query("INSERT INTO Plane set ?", newPlane, function(err, res){
    if(err){
      console.log("Model Plane - createPlane - Error : \n", err);
      result(err, null)
    }else{
      var date = new Date();
      var current_hour;
      if(date.getHours() < 10){
        current_hour = "0" + date.getHours() + ":";
      }else{
        current_hour = date.getHours() + ":";
      }
      if(date.getMinutes() < 10){
        current_hour += "0" + date.getMinutes() + ":";
      }else{
        current_hour += date.getMinutes() + ":";
      }
      if(date.getSeconds() < 10){
        current_hour += "0" + date.getSeconds();
      }else{
        current_hour += date.getSeconds();
      }

      var tweet = "L'avion " + newPlane.callsign ;

      getCompany(newPlane.callsign, tweet, current_hour);
    }
  });
};

// Route that get one plane with callsign
function getCompany(callsign, tweet, current_hour){
  sql.query("SELECT * FROM Plane WHERE callsign = ? LIMIT 1", callsign, function(err, response){
    if(err){
      console.log("Model Plane - getCompany - Error : \n", err);
      tweet += " est entré dans l'espace aérien nantais à " + current_hour;
      postTweet(tweet);
    }else{

      var sqljson = JSON.stringify(response);
      sqljson = JSON.parse(sqljson);
      console.log("SQL response : ", sqljson);
      if(sqljson[0]["company_name"] == null){
        tweet += " est entré dans l'espace aérien nantais à " + current_hour;
      }else{
        tweet += " de la compagnie " + sqljson[0]["company_name"] + " est entré dans l'espace aérien nantais à " + current_hour;
      }

      postTweet(tweet);
    }
  });
};

// Route that post tweet
function postTweet(tweet){
  request.post({
    "headers": { "content-type": "application/json" },
    "url": "http://localhost:8082/tweet/",
    "body": JSON.stringify({"content": tweet})
    }, (error, response, body) => {
      if(error){
        console.log("Model Plane - postTweet - Error : \n", error);
      } else{
        console.log("Model Plane - postTweet - Tweet successfully posted : " + tweet);
      }
  });
}

Plane.getPlane = function getPlane(params, result){
  sql.query("SELECT * FROM Plane WHERE callsign = ? AND DATE(date_flight)=? AND DATE_FORMAT(date_flight, '%H')=?", [params.callsign, params.date_flight, params.time], function(err, res){
    if(err){
      console.log("Model Plane - getPlane - Error : \n", err);
      result(err, null);
    }else{
      console.log("plane: ", res);
      result(null, res);
    }
  });
};

Plane.getAllPlanesByCompanies = function getAllPlanesByCompanies(result){
  sql.query("SELECT ifnull(company_name, 'Total') AS Company, "
    + "(SELECT count(callsign) FROM Plane WHERE date(date_flight) = date(now()) AND company_name = Company) AS Today, "
    + "(SELECT count(callsign) FROM Plane WHERE date(date_flight) = (date(now())-1) AND company_name = Company) AS Yesterday, "
    + "(SELECT count(callsign) FROM Plane WHERE week(date_flight) = week(now()) AND company_name = Company) AS Week, "
    + "(SELECT count(callsign) FROM Plane WHERE week(date_flight) = (week(now()) -1) AND company_name = Company) AS LastWeek, "
    + "(SELECT count(callsign) FROM Plane WHERE month(date_flight) = month(now()) AND company_name = Company) AS Month, "
    + "(SELECT count(callsign) FROM Plane WHERE month(date_flight) = (month(now()) -1) AND company_name = Company) AS LastMonth "
    + "FROM Plane "
    + "GROUP BY company_name "
    + "ORDER BY Today DESC, Week DESC, Month DESC", function(err, res){
    if(err){
      console.log("Model Plane - getAllPlanesByCompanies - Error : \n", err);
      result(err, null);
    } else {
      result(null, res);
    }
  });
};

Plane.getAllPlanesByTimeScale = function getAllPlanesByTimeScale(result){
  sql.query("SELECT "
    + "(SELECT count(callsign) FROM Plane WHERE date(date_flight) = date(now())) AS Today, "
    + "(SELECT count(callsign) FROM Plane WHERE date(date_flight) = (date(now())-1)) AS Yesterday, "
    + "(SELECT count(callsign) FROM Plane WHERE week(date_flight) = week(now())) AS Week, "
    + "(SELECT count(callsign) FROM Plane WHERE week(date_flight) = (week(now()) -1)) AS LastWeek, "
    + "(SELECT count(callsign) FROM Plane WHERE month(date_flight) = month(now())) AS Month, "
    + "(SELECT count(callsign) FROM Plane WHERE month(date_flight) = (month(now()) -1)) AS LastMonth ", function(err, res){
    if(err){
      console.log("Model Plane - getAllPlanesByTimeScale - Error : \n", err);
      result(err, null);
    } else {
      result(null, res);
    }
  });
};

module.exports = Plane;
