const express     = require('express'),
  app             = express(),
  router          = express.Router(),
  bodyParser      = require('body-parser'),
  port            = process.env.PORT || 8080;
var request = require("request");

var requestLoop = setInterval(function(){
  request({
      url: "https://opensky-network.org/api/states/all?lamin=46.84&lomin=-2.750&lamax=47.67&lomax=-0.492",
      method: "GET",
      timeout: 10000
  },function(error, response, body){
      if(!error && response.statusCode == 200){
          json = JSON.parse(body);
          var plane;
          var date = new Date();
          var month = "";
          if((date.getMonth()+1) < 10){
            month = "0" + (date.getMonth()+1);
          }else {
            month = (date.getMonth()+1)
          }
          var dateFlight = date.getFullYear() + "-" + month + "-" + date.getDate() + " " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
          for(var key in json["states"]){
            console.log("Key: " + key + " value : " + json["states"][key] + "\n");
            plane = json["states"][key];
            checkDB(plane, dateFlight);
          }
      }else{
          console.log('error get opensky - ' + error);
      }
  });
}, 5000);

function checkDB(plane, dateFlight){
  var date = new Date();
  var month = "";
  if((date.getMonth()+1) < 10){
    month = "0" + (date.getMonth()+1);
  }else {
    month = (date.getMonth()+1)
  }
  date = date.getFullYear() + "" + month + "" + date.getDate() + "/" + date.getHours();
  request.get({
    "headers": { "content-type": "application/json" },
    "url": "http://localhost:8081/plane/" + plane[1].trim() + "/" + date // 192.168.0.4
  }, (err, resp, bod) => {
      if(err){
        console.log(err, null);
      }else{
        console.log("Planes found : \n" + bod);
        if(bod == "[]"){
          postTweet(plane, dateFlight);
        }
      }
  });
}

function postTweet(plane, dateFlight){
  request.post({
    "headers": { "content-type": "application/json" },
    "url": "http://localhost:8081/planes/",
    "body": JSON.stringify({"callsign": plane[1].trim(),"date_flight": dateFlight,"origin_country": plane[2]})
  }, (er, res, bo) => {
      if(er){
        console.log("Error on post tweet : " + er);
      }
      console.log("Plane successfully added : " + plane[1]);
  });
}
