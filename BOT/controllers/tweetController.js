var Tweet = require('../model/tweetModel');

exports.postSimpleTweet = function(req, res){
  var newTweet = new Tweet(req.body);
  if(!newTweet.content){
    res.status(400).send({
      error: true,
      message: 'Tweet content is required'
    });
  }else{
    Tweet.postSimpleTweet(newTweet, function(err, tweet){
      if(err){
        res.send(err);
      }
      res.json(tweet);
    });
  }
};
