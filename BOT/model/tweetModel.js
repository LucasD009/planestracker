var twitterConnection = require('./twitterConnection');
var twitterAccount = twitterConnection.getTwitterConnection;

var Tweet = function(tweet){
  this.content = tweet.content;
};

Tweet.postSimpleTweet = function postSimpleTweet(newTweet, result){
  twitterAccount.post('statuses/update', {status: newTweet.content}, function(error, tweet, response) {
    if (!error) {
      console.log('Tweet has been post: ' + newTweet.content);
      result(null, response);
    }else{
      console.log('TWITTER error - postSimpleTweet: ', error);
    }
  });
};

module.exports = Tweet;
