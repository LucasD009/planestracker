module.exports = function(app){

  var tweetController = require('../controllers/tweetController');

  // Tweet routes
  app.route('/tweet')
    .post(tweetController.postSimpleTweet);
};
