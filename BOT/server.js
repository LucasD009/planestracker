const express     = require('express'),
  app             = express(),
  router          = express.Router(),
  bodyParser      = require('body-parser'),
  port            = process.env.PORT || 8082;

console.log('Server listening on port ' + port);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.listen(port);

var routes = require('./routes/routes');
routes(app);
